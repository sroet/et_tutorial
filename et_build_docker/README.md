The docker file here should be altered for every release of eT. It should then
be rebuild with:
```bash
docker build -t USER_NAME/et_binder:[version] .
```
followed by
```bash
docker push USER_NAME/et_binder:[version]
```

The Dockerfile in the main directory should be then updated to use the new one
as `FROM ...`
